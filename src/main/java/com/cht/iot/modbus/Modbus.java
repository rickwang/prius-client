package com.cht.iot.modbus;

public class Modbus {
	
	final static int EC_ERROR = 0x80;
	final static int EC_ILLEGAL_FUNCTION = 0x01;
	
	final static int FC_READ_COILS = 0x01;
	final static int FC_READ_DISCRETE_INPUTS = 0x02;
	final static int FC_WRITE_SINGLE_COIL = 0x05;
	final static int FC_WRITE_MULTIPLE_COILS = 0x0F;
	
	final static int FC_READ_INPUT_REGISTERS = 0x04;
	final static int FC_READ_HOLDING_REGISTERS = 0x03;
	final static int FC_WRITE_SINGLE_REGISTER = 0x06;
	final static int FC_WRITE_MULTIPLE_REGISTERS = 0x10;
	
	public static enum RegisterType {
		none, coil, discrete_input, input_register, holding_register  
	}
	
	public static enum WritingType {
		single, multiple
	}
}
