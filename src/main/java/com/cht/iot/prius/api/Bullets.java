package com.cht.iot.prius.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cht.iot.modbus.Modbus;
import com.cht.iot.prius.bean.SlaveProfile;

public interface Bullets {

	public static class Bullet {
		public static final DateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		public String op;
		public String timestamp;
		public Integer seq;
		
		public Bullet() {
			op = getClass().getSimpleName();
			timestamp = toTimestamp(System.currentTimeMillis());
		}
		
		public static final synchronized String toTimestamp(long ms) {
			return DF.format(new Date(ms));
		}
	}
	
	public static class Reply extends Bullet {
		public static final int SC_OK = 200;
		public static final int SC_FAILED = 400;
		
		public Integer sc;
		public String reason;
	}
	
	// ===
	
	public static class ErrorReply extends Reply {		
	}
	
	// ===
	
	public static class Introduce extends Bullet {
		public String version;
		public String salt;
	}
	
	// ===
	
	public static class ChallengeReq extends Bullet {
		public String digest;
	}
	
	public static class ChallengeReply extends Reply {		
	}
	
	// ===
	
	public static class EchoReq extends Bullet {
		public String message;
		
	}
	
	public static class EchoReply extends Reply {
		public String message;		
	}
	
	// ===
	
	public static class Heartbeat extends Bullet {		
	}
	
	// ===
	
	public static class OnSlaveArrived extends Bullet {
		public String id;
	}
	
	public static class OnSlaveExited extends Bullet {
		public String id;
	}
	
	// ===
	
	public static class GetConnectedSlaveIdListReq extends Bullet {		
	}
	
	public static class GetConnectedSlaveIdListReply extends Reply {
		public List<String> ids;
	}
	
	public static class GetManagedSlaveListReq extends Bullet {		
	}
	
	public static class GetManagedSlaveListReply extends Reply {
		public List<SlaveProfile> slaves;
	}
	
	public static class GetArrivedSlaveListReq extends Bullet {		
	}
	
	public static class GetArrivedSlaveListReply extends Reply {
		public List<SlaveProfile> slaves;
	}
	
	public static class GetSlavePerformanceReq extends Bullet {
		public String id;
	}
	
	public static class GetSlavePerformanceReply extends Reply {
		public Map<String, Object> performances;
	}
	
	// ===
	
	public static class ReadModbusRegistersReq extends Bullet {
		public String id;
		public Integer channel;
		public Modbus.RegisterType type;
		public Integer address;
		public Integer quantity;
	}
	
	public static class ReadModbusRegistersReply extends Reply {
		public int[] values;
	}
	
	// ===
	
	public static class WriteModbusSingleRegisterReq extends Bullet {
		public String id;
		public Integer channel;
		public Modbus.RegisterType type;
		public Integer address;
		public int value;
	}
	
	public static class WriteModbusSingleRegisterReply extends Reply {		
	}
	
	public static class WriteModbusMultipleRegistersReq extends Bullet {
		public String id;
		public Integer channel;
		public Modbus.RegisterType type;
		public Integer address;
		public int[] values;
	}
	
	public static class WriteModbusMultipleRegistersReply extends Reply {		
	}
	
	// ===
	
	public static class ReadValueReq extends Bullet {
		public String id;
		public String device;
		public String sensor;
	}
	
	public static class ReadValueReply extends Reply {
		public String last;
		public String value;
	}
	
	public static class AskValueReq extends ReadValueReq {		
	}
	
	// ===
	
	public static class WriteValueReq extends Bullet {
		public String id;
		public String device;
		public String sensor;
		public String value;
	}
	
	public static class WriteValueReply extends Reply {
	}
	
	// ===
	
	public static class OnValueChanged extends Bullet {
		public String id;
		public String device;
		public String sensor;
		public String last;
		public String value;
	}
	
	// ===
}
