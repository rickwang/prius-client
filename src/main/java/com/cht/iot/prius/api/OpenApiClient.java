package com.cht.iot.prius.api;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.cht.iot.prius.api.Bullets.Bullet;
import com.cht.iot.prius.api.Bullets.Reply;

public interface OpenApiClient {
	
	boolean isReady();
	
	void waitForReady(long timeout) throws InterruptedException, TimeoutException;
	
	void setListener(Listener listener);
	
	void send(Bullet req) throws IOException;
	
	<R extends Reply> R invoke(Bullet req, Class<R> clazz, long timeout) throws IOException, InterruptedException, TimeoutException;

	public static interface Listener {
		void onMessage(Bullet bullet);
	}
}
