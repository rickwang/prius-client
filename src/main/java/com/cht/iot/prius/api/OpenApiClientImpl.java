package com.cht.iot.prius.api;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.cht.iot.prius.api.Bullets.*;

import com.cht.iot.util.JsonUtils;

@ClientEndpoint
public class OpenApiClientImpl implements OpenApiClient {
	static final Logger LOG = LoggerFactory.getLogger(OpenApiClientImpl.class);
	
	static final long RECONNECTION_DELAY = 3000L;
	
	final String uri;
	String key = "iiot";

	ScheduledExecutorService executor = Executors.newScheduledThreadPool(10);
	
	Session session;
	
	int seq = 0;
	
	boolean ready = false;
		
	Map<Integer, RequestFuture> requests = Collections.synchronizedMap(new HashMap<Integer, RequestFuture>()); // seq -> RequestFuture
	
	Listener listener = new Listener() {
		public void onMessage(Bullet bullet) {};
	};
	
	public OpenApiClientImpl(String uri) {
		this.uri = uri;
	}
	
	public synchronized boolean isReady() {
		return ready;
	}
	
	public synchronized void waitForReady(long timeout) throws InterruptedException, TimeoutException {
		if (!ready) {
			wait(timeout);
		}
		
		if (!ready) {
			throw new TimeoutException("timeout");
		}
	}
	
	public synchronized void setReady(boolean ready) {
		this.ready = ready;
		
		notifyAll();
	}	
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public void setListener(Listener listener) {
		this.listener = listener;
	}
	
	// ======
	
	public void start() {
		reconnect(0);
	}
	
	protected void reconnect(long delay) {
		ready = false;
		
		if (!executor.isShutdown()) {		
			executor.schedule(new Runnable() {
				public void run() {
					doConnect();				
				}			
			}, delay, TimeUnit.MILLISECONDS);
		}
	}
	
	protected void doConnect() {
		try {
			WebSocketContainer wsc = ContainerProvider.getWebSocketContainer();			
			wsc.connectToServer(this, new URI(uri));
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	@OnOpen
	public synchronized void onOpen(Session session) {
		this.session = session;
		
		LOG.info("Connected - " + session);
	}
	
	@OnError
	public void onError(Session session, Throwable thr) {		
		LOG.info("Error", thr);
	}
	
	@OnClose
	public void onClose(Session session, CloseReason reason) {
		LOG.info("Closed - " + reason);
		
		setReady(false);
		
		reconnect(RECONNECTION_DELAY);
	}
	
	@OnMessage
	public void onMessage(String json) {
		LOG.info("{}", json);
		
		try {				
			String op = JsonUtils.getField(json, "op");
			if (Introduce.class.getSimpleName().equals(op)) {
				Introduce req = JsonUtils.fromJson(json, Introduce.class);
				String digest = new String(Hex.encodeHex(DigestUtils.md5(req.salt + key)));
				
				ChallengeReq cr = new ChallengeReq();
				cr.digest = digest;
				
				OpenApiClientImpl.this.send(cr);
				
			} else if (ChallengeReply.class.getSimpleName().equals(op)) {
				setReady(true);
				
			} else {					
				Class<?> clazz = Class.forName(String.format("%s$%s", Bullets.class.getName(), op));
				Bullet bullet = (Bullet) JsonUtils.fromJson(json, clazz);					
				RequestFuture rf = requests.get(bullet.seq);
				if (rf != null) {
					rf.setValue(bullet);
					
				} else {
					listener.onMessage(bullet);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	public void stop() throws IOException {
		executor.shutdown();
		
		if (isReady()) {
			session.close();
		}
	}
	
	// ======
		
	public void send(Bullet req) throws IOException {
		req.seq = seq++;
		
		String json = JsonUtils.toJson(req);
		
		LOG.info("{}", json);
		
		session.getAsyncRemote().sendText(json);			
	}
	
	@SuppressWarnings("unchecked")
	public <R extends Reply> R invoke(Bullet req, Class<R> clazz, long timeout) throws IOException, InterruptedException, TimeoutException {
		RequestFuture rf = new RequestFuture();
		requests.put(seq, rf);
		try {
			send(req);
			
			return (R) rf.getValue(timeout);
			
		} finally {
			requests.remove(seq);
		}		
	}
	
	// ======
	
	static class RequestFuture {
		Object value;

		public RequestFuture() {
		}

		public synchronized void reset() {
			value = null;
		}

		public synchronized void setValue(Object value) {
			this.value = value;
			
			notifyAll();
		}

		public synchronized Object getValue(long timeout) throws InterruptedException, TimeoutException {
			if (value == null) {
				wait(timeout);
			}
			
			if (value == null) {
				throw new TimeoutException();
			}
			
			return value;
		}
	}
}
