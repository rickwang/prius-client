package com.cht.iot.prius.bean;

public class Alias {
	String value;
	String alias;
	
	public Alias() {
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}
}
