package com.cht.iot.prius.bean;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DeviceProfile {
	String name;
	String desc;
	String type;
	Map<String, String> attributes;	// extra information
	
	@JsonIgnore	Integer channel;	// Modbus Slave ID
	@JsonIgnore	Long interval;		// polling interval in ms
	@JsonIgnore	Long timeout;		// responding timeout in ms
	
	List<SensorProfile> sensors;
	
	public DeviceProfile() {
	}	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
	
	public Integer getChannel() {
		return channel;
	}
	
	public void setChannel(Integer channel) {
		this.channel = channel;
	}
	
	public Long getInterval() {
		return interval;
	}
	
	public void setInterval(Long interval) {
		this.interval = interval;
	}
	
	public Long getTimeout() {
		return timeout;
	}
	
	public void setTimeout(Long timeout) {
		this.timeout = timeout;
	}

	public List<SensorProfile> getSensors() {
		return sensors;
	}

	public void setSensors(List<SensorProfile> sensors) {
		this.sensors = sensors;
	}
}
