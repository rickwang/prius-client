package com.cht.iot.prius.bean;

public enum Direction {
	AI, AO, DI, DO
}
