package com.cht.iot.prius.bean;

public class Range {
	String min;
	String max;
	
	public Range() {	
	}
	
	public String getMin() {
		return min;
	}
	
	public void setMin(String min) {
		this.min = min;
	}
	
	public String getMax() {
		return max;
	}
	
	public void setMax(String max) {
		this.max = max;
	}
}
