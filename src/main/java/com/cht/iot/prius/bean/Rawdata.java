package com.cht.iot.prius.bean;

public class Rawdata {
	public final String id;
	public final String deviceName;
	public final String sensorName;
	
	public final long timestamp;	
	public final String value;
	
	public Rawdata(String id, String deviceName, String sensorName, String value) {
		this.id = id;
		this.deviceName = deviceName;
		this.sensorName = sensorName;
		this.timestamp = System.currentTimeMillis();
		this.value = value;
	}
	
	public boolean isChanged(String value, float deadband) {
		if (!this.value.equals(value)) {
			return true;
		}
		
		return false;
	}
}
