package com.cht.iot.prius.bean;

import java.util.List;
import java.util.Map;

import com.cht.iot.modbus.Modbus;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class SensorProfile {
	String name;
	String desc;
	SensorType type;
	Direction direction; // AI, AO, DI, DO
	Range range;
	String unit;
	Map<String, String> attributes;	// extra information
		
	@JsonIgnore Boolean readable = true; // some DO register cannot be read
	@JsonIgnore Modbus.RegisterType register;
	@JsonIgnore Modbus.WritingType writing = Modbus.WritingType.single;
	@JsonIgnore Integer address; // 0 base addressing
	@JsonIgnore Integer quantity = 1;
	@JsonIgnore Integer preloads; // pre-loading registers
	@JsonIgnore String r2v; // formula - from register to value
	@JsonIgnore String v2r; // formula - from value to register
	@JsonIgnore Double throttle; // determine the value changed
			
	List<Alias> aliases;
	
	public SensorProfile() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public SensorType getType() {
		return type;
	}

	public void setType(SensorType type) {
		this.type = type;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	
	public Range getRange() {
		return range;
	}
	
	public void setRange(Range range) {
		this.range = range;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
	
	public Boolean isReadable() {
		return readable;
	}
	
	public void setReadable(Boolean readable) {
		this.readable = readable;
	}

	public Modbus.RegisterType getRegister() {
		return register;
	}
	
	public void setRegister(Modbus.RegisterType register) {
		this.register = register;
	}	
	
	public Modbus.WritingType getWriting() {
		return writing;
	}
	
	@JsonIgnore
	public boolean isSingleWriting() {
		return ((writing == null) || (writing == Modbus.WritingType.single));
	}
	
	public void setWriting(Modbus.WritingType writing) {
		this.writing = writing;
	}

	public int getAddress() {
		return address;
	}

	public void setAddress(int address) {
		this.address = address;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public Integer getPreloads() {
		return preloads;
	}
	
	public void setPreloads(Integer preloads) {
		this.preloads = preloads;
	}
	
	public String getR2v() {
		return r2v;
	}
	
	public void setR2v(String r2v) {
		this.r2v = r2v;
	}
	
	public String getV2r() {
		return v2r;
	}
	
	public void setV2r(String v2r) {
		this.v2r = v2r;
	}
	
	public Double getThrottle() {
		return throttle;
	}
	
	public void setThrottle(Double throttle) {
		this.throttle = throttle;
	}

	public List<Alias> getAliases() {
		return aliases;
	}

	public void setAliases(List<Alias> aliases) {
		this.aliases = aliases;
	}
}
