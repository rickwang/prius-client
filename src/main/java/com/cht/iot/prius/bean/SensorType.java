package com.cht.iot.prius.bean;

public enum SensorType {
	Switch, Gauge, Counter, Text
}
