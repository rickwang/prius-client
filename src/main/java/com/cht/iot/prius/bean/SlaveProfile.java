package com.cht.iot.prius.bean;

import java.util.List;

public class SlaveProfile {
	String id;
	
	Protocol protocol;
	
	List<DeviceProfile> devices;
	
	public SlaveProfile() {
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Protocol getProtocol() {
		return protocol;
	}
	
	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}
	
	public List<DeviceProfile> getDevices() {
		return devices;
	}
	
	public void setDevices(List<DeviceProfile> devices) {
		this.devices = devices;
	}
}
