package com.cht.iot.prius.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.iot.modbus.Modbus;
import static com.cht.iot.prius.api.Bullets.*;

import java.util.List;

import com.cht.iot.util.JsonUtils;

public class OpenApiClientTest implements OpenApiClient.Listener {
	static final Logger LOG = LoggerFactory.getLogger(OpenApiClientImpl.class);

//	String uri = "ws://localhost:8080/api";
//	String uri = "ws://192.168.0.67:8080/api";
//	String uri = "ws://192.168.31.176:8080/api";
	String uri = "ws://ien:18080/api";
	
	String key = "iiot";
	
	long timeout = 10000L;
	
	public void testEcho(OpenApiClientImpl client) throws Exception {
		EchoReq req = new EchoReq();
		
		EchoReply reply = client.invoke(req, EchoReply.class, timeout);
	}
	
	public void testReadModbusRegisters(OpenApiClientImpl client) throws Exception {
		ReadModbusRegistersReq req = new ReadModbusRegistersReq();
//		req.id = "COTH000001";
		req.id = "PMTH000001";
		req.channel = 1;
		req.type = Modbus.RegisterType.holding_register;
		req.address = 0;
		req.quantity = 5;
		
		ReadModbusRegistersReply reply = client.invoke(req, ReadModbusRegistersReply.class, timeout);
	}
	
	public void testWriteModbusMultipleRegisters(OpenApiClientImpl client, int value) throws Exception {
		WriteModbusMultipleRegistersReq req = new WriteModbusMultipleRegistersReq();
		req.id = "00Y0EUT3SFR034PF";
		req.channel = 1;
		req.type = Modbus.RegisterType.holding_register;
		req.address = 16384;
		req.values = new int[] { value };
		
		WriteModbusMultipleRegistersReply reply = client.invoke(req, WriteModbusMultipleRegistersReply.class, timeout);
	}
	
	public void testWriteModbusSingleRegister(OpenApiClientImpl client, int value) throws Exception {
		WriteModbusSingleRegisterReq req = new WriteModbusSingleRegisterReq();
		req.id = "ACLED000001";
		req.channel = 1;
		req.type = Modbus.RegisterType.holding_register;
		req.address = 7;
		req.value = value;
		
		WriteModbusSingleRegisterReply reply = client.invoke(req, WriteModbusSingleRegisterReply.class, timeout);
	}
	
//	public void testReadHoldingRegisters(OpenApiClientImpl client) throws Exception {
//		ReadHoldingRegistersReq req = new ReadHoldingRegistersReq();
//		req.id = "00Y0EUT3SFR034PF";
//		req.channel = 1;
//		req.address = 16384;
//		req.quantity = 1;
//		
//		ReadHoldingRegistersReply reply = client.request(req, ReadHoldingRegistersReply.class, timeout);
//	}
//	
//	public void testWriteMultipleRegisters(OpenApiClientImpl client, int value) throws Exception {
//		WriteMultipleRegistersReq req = new WriteMultipleRegistersReq();
//		req.id = "00Y0EUT3SFR034PF";
//		req.channel = 1;
//		req.address = 16384;
//		req.values = new int[] { value };
//		
//		WriteMultipleRegistersReply reply = client.request(req, WriteMultipleRegistersReply.class, timeout);
//	}
	
	public void testReadValue(OpenApiClientImpl client) throws Exception {
		ReadValueReq req = new ReadValueReq();
		req.id = "00Y0EUT3SFR034PF";
		req.device = "SmartPlug";
		req.sensor = "V";
		
		ReadValueReply reply = client.invoke(req, ReadValueReply.class, timeout);		
	}
	
	public void testAskValue(OpenApiClientImpl client) throws Exception {
		AskValueReq req = new AskValueReq();
		req.id = "00Y0EUT3SFR034PF";
		req.device = "SmartPlug";
		req.sensor = "V";
		
		client.send(req);
	}
	
	public void testWriteValue(OpenApiClientImpl client, String sensorName, String value) throws Exception {
		WriteValueReq req = new WriteValueReq();
		req.id = "00Y0EUT3SFR034PF";
		req.device = "SmartPlug";
		req.sensor = sensorName;
		req.value = value;
		
		WriteValueReply reply = client.invoke(req, WriteValueReply.class, timeout);		
	}
	
	public void testGetManagedSlaveList(OpenApiClientImpl client) throws Exception {
		GetManagedSlaveListReq req = new GetManagedSlaveListReq();
		
		GetManagedSlaveListReply reply = client.invoke(req, GetManagedSlaveListReply.class, timeout);		
	}
	
	public void testGetArrivedSlaveList(OpenApiClientImpl client) throws Exception {
		GetArrivedSlaveListReq req = new GetArrivedSlaveListReq();
		
		GetArrivedSlaveListReply reply = client.invoke(req, GetArrivedSlaveListReply.class, timeout);		
	}
	
	public List<String> testGetConnectedSlaveIdList(OpenApiClientImpl client) throws Exception {
		GetConnectedSlaveIdListReq req = new GetConnectedSlaveIdListReq();
		
		GetConnectedSlaveIdListReply reply = client.invoke(req, GetConnectedSlaveIdListReply.class, timeout);
		return reply.ids;
	}
	
	public void testGetSlavePerformance(OpenApiClientImpl client) throws Exception {
		List<String> ids = testGetConnectedSlaveIdList(client);
		if (ids != null) {
			for (String id : ids) {
				GetSlavePerformanceReq req = new GetSlavePerformanceReq();
				req.id = id;
				
				GetSlavePerformanceReply reply = client.invoke(req, GetSlavePerformanceReply.class, timeout);
			}
		}
	}
	
	public void onMessage(Bullet bullet) {		
	}
	
	public void test() throws Exception {
		OpenApiClientImpl client = new OpenApiClientImpl(uri);
		client.setKey(key);
		client.setListener(this);
		client.start();
		try {		
			client.waitForReady(timeout);
			
//			testEcho(client);
			
//			testReadModbusRegisters(client);
//			testWriteModbusMultipleRegisters(client, 1);
//			testWriteModbusSingleRegister(client, 0);
			
//			testReadHoldingRegisters(client);
//			
//			testWriteMultipleRegisters(client, 1);
//			
//			testReadValue(client);
//			testAskValue(client);
			
//			testWriteValue(client, "Switch", "1");
//			testWriteValue(client, "Switch", "0");
//			testWriteValue(client, "Reset", "1");
			
			testGetManagedSlaveList(client);
//			testGetArrivedSlaveList(client);
//			testGetConnectedSlaveIdList(client);
//			testGetSlavePerformance(client);
			
//			Thread.sleep(2000L);
//			
//			testWriteMultipleRegisters(client, 0);
			
			Object lck = new Object();
			synchronized (lck) {
				lck.wait();
			}
		} finally {		
			client.stop();
		}
	}
	
	public static void main(String[] args) throws Exception {
		OpenApiClientTest t = new OpenApiClientTest();
		t.test();
	}
}
